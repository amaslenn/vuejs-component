# vuejs-component

> A Vue.js project based on [this tutorial](https://medium.freecodecamp.org/build-your-first-vue-js-component-2dc204bca514)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
